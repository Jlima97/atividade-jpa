package br.com.jean.app;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.jean.domain.Departamento;
import br.com.jean.domain.Funcionario;
import br.com.jean.domain.TipoFuncionarioEnum;

public class Aplicacao {
	
	public static void main(String[] args) {
	
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Atividade-JPA");
		EntityManager em = emf.createEntityManager();
		
		Departamento d1 = new Departamento("2343", "TI");
		Date data = new Date();
		
		Funcionario f1 = new Funcionario(1345, "Jean", "0004705", d1, TipoFuncionarioEnum.FXO, data, null);
		Funcionario f2 = new Funcionario(3213, "claudio", "430004705", d1, TipoFuncionarioEnum.FXO, data, null);


		em.getTransaction().begin();
		em.persist(d1);
		em.persist(f1);
		em.persist(f2);
		em.getTransaction().commit();
		
		em.close();
		emf.close();
		
	}
	
		
	
}
