package br.com.jean.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Departamento {
	@Id
	@Column(nullable = false, columnDefinition="CHAR(5)")
private String codigo;
@Column(length = 40, nullable = false)
private String nome;

@OneToMany(mappedBy = "departamento")
private List<Funcionario> funcionarios = new ArrayList<Funcionario>();

public Departamento() {}

public Departamento(String codigo, String nome) {
	super();
	this.codigo = codigo;
	this.nome = nome;
}

public String getCodigo() {
	return codigo;
}

public void setCodigo(String codigo) {
	this.codigo = codigo;
}

public String getNome() {
	return nome;
}

public void setNome(String nome) {
	this.nome = nome;
}

public List<Funcionario> getFuncionarios() {
	return funcionarios;
}

public void setFuncionarios(List<Funcionario> funcionarios) {
	this.funcionarios = funcionarios;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Departamento other = (Departamento) obj;
	if (codigo == null) {
		if (other.codigo != null)
			return false;
	} else if (!codigo.equals(other.codigo))
		return false;
	return true;
}

@Override
public String toString() {
	return "Departamento [codigo=" + codigo + ", nome=" + nome + ", funcionarios=" + funcionarios + "]";
}





}
