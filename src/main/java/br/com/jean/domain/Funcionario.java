package br.com.jean.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Funcionarios")
public class Funcionario {
@Id
@Column(nullable = false)
private Integer matricula;
@Column(name = "funcionario_nome", length = 40, nullable = false)
private String nome;
@Column(columnDefinition="CHAR(11)", nullable = false, unique = true)
private String cpf;

@ManyToOne
@JoinColumn(name = "departamento_id", nullable = false)
private Departamento departamento;

@Enumerated(EnumType.STRING)
@Column(nullable = false, columnDefinition="CHAR(3)")
private TipoFuncionarioEnum tipo;
@Column(nullable = false)
private Date data_admissao;
@Column(nullable = true)
private Date data_demissao;
	
public Funcionario() {}

public Funcionario(Integer matricula, String nome, String cpf, Departamento departamento, TipoFuncionarioEnum tipo,
		Date dataAdmissao, Date dataDemissao) {
	super();
	this.matricula = matricula;
	this.nome = nome;
	this.cpf = cpf;
	this.departamento = departamento;
	this.tipo = tipo;
	this.data_admissao = dataAdmissao;
	this.data_demissao = dataDemissao;
}

public Integer getMatricula() {
	return matricula;
}

public void setMatricula(Integer matricula) {
	this.matricula = matricula;
}

public String getNome() {
	return nome;
}

public void setNome(String nome) {
	this.nome = nome;
}

public String getCpf() {
	return cpf;
}

public void setCpf(String cpf) {
	this.cpf = cpf;
}

public Departamento getDepartamento() {
	return departamento;
}

public void setDepartamento(Departamento departamento) {
	this.departamento = departamento;
}

public TipoFuncionarioEnum getTipo() {
	return tipo;
}

public void setTipo(TipoFuncionarioEnum tipo) {
	this.tipo = tipo;
}

public Date getDataAdmissao() {
	return data_admissao;
}

public void setDataAdmissao(Date dataAdmissao) {
	this.data_admissao = dataAdmissao;
}

public Date getDataDemissao() {
	return data_demissao;
}

public void setDataDemissao(Date dataDemissao) {
	this.data_demissao = dataDemissao;
}

@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((cpf == null) ? 0 : cpf.hashCode());
	result = prime * result + ((matricula == null) ? 0 : matricula.hashCode());
	return result;
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Funcionario other = (Funcionario) obj;
	if (cpf == null) {
		if (other.cpf != null)
			return false;
	} else if (!cpf.equals(other.cpf))
		return false;
	if (matricula == null) {
		if (other.matricula != null)
			return false;
	} else if (!matricula.equals(other.matricula))
		return false;
	return true;
}

@Override
public String toString() {
	return "Funcionario [matricula=" + matricula + ", nome=" + nome + ", cpf=" + cpf + ", departamento=" + departamento
			+ ", tipo=" + tipo + ", dataAdmissao=" + data_admissao + ", dataDemissao=" + data_demissao + "]";
}



	
}
